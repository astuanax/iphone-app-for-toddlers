angular.module('starter.config',[])
    .constant('AnimalsConfig', {
        'file': 'json/animals.json'
    })
    .factory('AnimalFactory', function($http, $q, AnimalsConfig) {

        return {
            getAnimals: function() {
                return $http.get(AnimalsConfig.file).then(function(result) {
                    return result.data;
                })
            }
        }
        // var animals = function() {
        //         $http.get(AnimalsConfig.file).success(function(data) {
        //
        //             return data
        //         });
        //     };
        //
        //     return {
        //         all: function() {
        //             return animals;
        //         },
        //         get: function(name) {
        //             return animals[name];
        //         }
        //     }
    })
.controller('HabitatCtrl',function($scope, $state, $ionicSlideBoxDelegate,AnimalFactory) {
    $scope.animals = [];
    AnimalFactory.getAnimals().then(function(data) {
           $scope.animals = data;
           $ionicSlideBoxDelegate.update();
    });

   
    
});